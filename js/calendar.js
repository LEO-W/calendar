$(document).ready(function() {
	/*
		BUILD CALENDAR
	*/
	$currentYear = new Date().getFullYear();

	$monthNum = new Date().getMonth();
	var months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
	$currentMonth = months[$monthNum];

	var weekday = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
	$today = new Date();
	$turner = 1;
	$turner2 = 1;

	$selectYear = $currentYear;
	$selectMonth = $monthNum;

	function upDate($month, $year) {
		$selectMonth = $month;
		$selectYear = $year;
		$turner = 1;
		$turner2 = 1;
		buildCalendar($selectMonth, $selectYear);
		return $selectMonth;
		return $selectYear;
	}

	function back($month, $year) {
		$month = parseInt($month) - 1;
		if ($month<0) {
			$month = 11;
			$year = $year - 1;
		}
		upDate($month, $year);
	}

	$('.back').click(function() {
		back($selectMonth, $selectYear);
	});

	function forth($month, $year) {
		$month = parseInt($month) + 1;
		if ($month>11) {
			$month = 0;
			$year = $year + 1;
		}
		upDate($month, $year);
	}

	$('.forth').click(function() {
		forth($selectMonth, $selectYear);
	});

	$('.today').click(function() {
		upDate($monthNum, $currentYear);
	});

	function buildCalendar($month, $year) {
		// alert('month: '+$month+' und Year: '+$year);
		$firstDay = (new Date($year, $month)).getDay();
		// alert($weekday[$firstDay]);
		$toda = weekday[new Date().getDay()];
		// alert($toda);
		$daysInMonth = 32 - new Date($year, $month, 32).getDate();
		// alert('ym: '+$daysInMonth);

		$('#calendarjs .calendar-head .year').html($year);
		$('#calendarjs .calendar-head .month').html(months[$month]);

		$tbl = $('.date-body');
		$tbl.html("");

		// alert(weekday[$firstDay]);
		$date = 1;
		// alert('date: '+$date);
		for (var i = 0; i < 6	; i++) {
			var $row = document.createElement('tr');
			$row.classList.add('calendar-row');

			for (var j = 0; j < 7; j++) {
				if ($turner == true && $firstDay != 0) {
						j++;

					if (i === 0 && j < $firstDay) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						if ($turner2 == true && $firstDay == 6) {
							$cell.remove();
							$turner2 = 0;
						} else if ($turner2 == true && $firstDay == 4) {
							// alert(weekday[$firstDay]);
							$cell.remove();
							j = j - 1;
							$turner2 = 0;
						} else if ($turner2 == true && $firstDay == 2) {
							$cell.remove();
							j = j - 1;
							$turner2 = 0;
						}
					} else if ($date > $daysInMonth) {
						break;
					} else {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode($date);
						if ($date === $today.getDate() && $year === $today.getFullYear() && $month === $today.getMonth()) {
	                    	$cell.classList.add("todayday");
	                	}
						$cell.append($cellVal);
						$row.append($cell);
						$date++;
						$turner = 0;
					}
				}
				
				if (i === 0 && j < $firstDay) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
					} else if (i === 0 && $firstDay == 0 && $turner2 == true) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						j = 5;
						$turner2 = 0;
					} else if ($date > $daysInMonth) {
						break;
					} else {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode($date);
						if ($date === $today.getDate() && $year === $today.getFullYear() && $month === $today.getMonth()) {
	                    	$cell.classList.add("todayday");
	                	}
						$cell.append($cellVal);
						$row.append($cell);
						$date++;
					}
				// var $cell = document.createElement('td');
				
			}

			$tbl.append($row);
			// alert('i: '+i);
		}
	}

	buildCalendar($selectMonth, $selectYear);
});