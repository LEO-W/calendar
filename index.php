<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Termione</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/calendar.js"></script>
</head>
<body>
	<h1>Termione</h1>
<?php
	// $d = new DateTime('2019-04');
    // $d->modify('first day of this month');
    // echo $d->format('N');

    // $currentYear = date('Y');
    // $monthNum = date('n');
    // $daysInMonth = cal_days_in_month (CAL_GREGORIAN, $monthNum, $currentYear);
    // echo 'popopo -- '.$daysInMonth;
?>
<div id="calendar-wrapper">
	<div id="calendarjs">
		<div class="calendar-head">
			<span class="back"><<</span>
			<span class="month"></span>
			<span class="year"></span>
			<span class="forth">>></span>
			<span class="today">Heute</span>
		</div>
		<table class="calendar-body">
			<thead>
				<tr class="calendar-row">
	                <th class="calendar-cell" id="dayMo0">Montag</th>
	                <th class="calendar-cell" id="dayTu0">Dienstag</th>
	                <th class="calendar-cell" id="dayWe0">Mittwoch</th>
	                <th class="calendar-cell" id="dayTh0">Donnerstag</th>
	                <th class="calendar-cell" id="dayFr0">Freitag</th>
	                <th class="calendar-cell" id="daySa0">Samstag</th>
	                <th class="calendar-cell" id="daySu0">Sonntag</th>
	            </tr>
			</thead>
			<tbody class="date-body">
				
			</tbody>
		</table>
	</div>

	<!-- PHP  CALENDAR -->
	<?php 
		require "calendar-function.php";
	?>

	<div id="calendarphp">
		<div class="calendar-head">
			<span class="back"><<</span>
			<span class="yearMonth"><?= $monthArr[$selectMonth].' '.$selectYear; ?></span>
			<span class="forth">>></span>
			<span class="today">Heute</span>
		</div>
		<table class="calendar-body">
			<thead>
				<tr class="calendar-row">
	                <th class="calendar-cell" id="dayMo0">Montag</th>
	                <th class="calendar-cell" id="dayTu0">Dienstag</th>
	                <th class="calendar-cell" id="dayWe0">Mittwoch</th>
	                <th class="calendar-cell" id="dayTh0">Donnerstag</th>
	                <th class="calendar-cell" id="dayFr0">Freitag</th>
	                <th class="calendar-cell" id="daySa0">Samstag</th>
	                <th class="calendar-cell" id="daySu0">Sonntag</th>
	            </tr>
			</thead>
			<tbody class="cal-body">
				<?php
					
					buildCalendar($selectMonth, $selectYear);

				?>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>