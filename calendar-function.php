<?php

$currentYear = date('Y');

$monthNum = date('n');
$monthArr = ['fill_0','Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
$currentMonth = $monthArr[$monthNum];

$weekday = ['fill_0', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
$turner = 1;
$turner2 = 1;

$selectYear = $currentYear;
$selectMonth = $monthNum;

function upDate($month, $year) {
	$selectMonth = $month;
	$selectYear = $year;
	$turner = 1;
	$turner2 = 1;
	buildCalendar($selectMonth, $selectYear);
	return $selectMonth;
	return $selectYear;
}

if (isset($_POST['today'])) {
	upDate($monthNum, $currentYear);
}

function back($month, $year) {
	$month = parseInt($month) - 1;
	if ($month<0) {
		$month = 11;
		$year = $year - 1;
	}
	upDate($month, $year);
}

if (isset($_POST['back'])) {
	back($selectMonth, $selectYear);
}

function forth($month, $year) {
	$month = parseInt($month) + 1;
	if ($month>11) {
		$month = 0;
		$year = $year + 1;
	}
	upDate($month, $year);
}

if (isset($_POST['forth'])) {
	forth($selectMonth, $selectYear);
}

function buildCalendar($month, $year) {
	$thatDate = new DateTime($year.'-'.$month);

	$firstDay = $thatDate;
	$firstDay->modify('first day of this month');
    $firstDay = $firstDay->format('N');

	$daysInMonth = cal_days_in_month (CAL_GREGORIAN, $month, $year);

	// CALENDAR HEAD DISPLAY $month AND $year

	$date = 1;

	for ($i = 1; $i < 7; $i++) {
		echo "<tr class='calendar-row'>";
			for ($j = 1; $j < 8; $j++) {

				if ($i == 1 && $j < $firstDay) {
					echo "<td class='calendar-cell'>";
						echo "";
					echo "</td>";
				} else if ($date > $daysInMonth) {
					break;
				} else {
					if ($date == date('j') && $year == date('Y') && $month == date('n')) {
						echo "<td class='calendar-cell todayday'>";
					} else {
						echo "<td class='calendar-cell'>";
					}
					echo $date;
					echo "</td>";
					$date++;
				}
			}
		echo "</tr>";
	}

}

// buildCalendar($selectMonth, $selectYear);